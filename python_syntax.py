skill_completed = "Python Syntax"
exercises_completed = 13
#The amount of points for each exercise may change, because points don't exist yet
points_per_exercise = 5
point_total = 100
point_total += exercises_completed*points_per_exercise

print("I got "+str(point_total)+" points!")

float_1 = 0.25
float_2 = 40.0

product = float_1 * float_2
big_string = "The product was " + str(product)

# Hi! I'm Maria and I live in script.py.
# I'm an expert Python coder.
# I'm 21 years old and I plan to program cool stuff forever.

age_is = int(input("What is my age?"))
if(age_is == 12): 
    print("True")
else:
    print("False")

haiku = '''
The old pond,
A frog jumps in:
Plop!
'''

cucumbers = 100
num_people = 6

whole_cucumbers_per_person = cucumbers/num_people
print(whole_cucumbers_per_person)

float_cucumbers_per_person = float(cucumbers)/num_people
print(float_cucumbers_per_person)

city_name = "St. Potatosburg"
# it`s the population of the city
city_pop = 340000

january_to_june_rainfall = 1.93 + 0.71 + 3.53 + 3.41 + 3.69 + 4.50
annual_rainfall = january_to_june_rainfall

july_rainfall = 1.05
annual_rainfall += july_rainfall

august_rainfall = 4.91
annual_rainfall += august_rainfall

september_rainfall = 5.16
annual_rainfall += september_rainfall

october_rainfall = 7.20
annual_rainfall += october_rainfall

november_rainfall = 5.06
annual_rainfall += november_rainfall

december_rainfall = 4.06
annual_rainfall += december_rainfall
print(annual_rainfall)

product = 3*4
remainder = 1398 % 11

todays_date = "January 28, 2023"

print("How do you make a hot dog stand?")
print("You take away its chair!")

print("Hello" + " " + "Pranjal")
print("Hello World!")
print("Deep into distant woodlands winds a mazy way, reaching to overlapping spurs of mountains bathed in their hill-side blue.")
print("Hello, World")
print("Water; there is not a drop of water there! Were Niagara but a cataract of sand, would you travel your thousand miles to see it?")
