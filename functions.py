def tax(bill):
  """Adds 8% tax to a restaurant bill."""
  bill *= 1.08
  print "With tax: %f" % bill
  return bill

def tip(bill):
  """Adds 15% tip to a restaurant bill."""
  bill *= 1.15
  print "With tip: %f" % bill
  return bill
  
meal_cost = 100
meal_with_tax = tax(meal_cost)
meal_with_tip = tip(meal_with_tax)

def spam():
    """This function prints the string "Eggs!" to the console."""
    print("Eggs!")

# Call the function
spam()

def square(n):
    """Returns the square of a number."""
    squared = n ** 2
    print("%d squared is %d.", %(n, squared))
    return squared

square(4)

def power(___, ___):  # Add your parameters here!
  result = base ** exponent
  print "%d to the power of %d is %d." % (base, exponent, result)

power(__, __)  # Add your arguments here!

def one_good_turn(n):
    return n+1

def deserves_another(n):
    return n+2

def cube(number):
    """This function returns the cube of the given number."""
    return number ** 3

def by_three(number):
    """This function checks if the number is divisible by 3. If yes, it returns the cube of the number; otherwise, it returns False."""
    if number % 3 == 0:
        return cube(number)
    else:
        return False

# Example usage:
result1 = by_three(9)  # Since 9 is divisible by 3, it returns the cube of 9.
result2 = by_three(7)  # Since 7 is not divisible by 3, it returns False.

print(result1)  # Output: 729 (cube of 9)
print(result2)  # Output: False

from math import math
import math
number = 25
result = math.sqrt(number)
print("Square root of", number, "is:", result)
# print sqrt(25)

from math import *

# Using various math functions
number = 25
result_sqrt = sqrt(number)
result_pow = pow(2, 3)  # Example of using pow() function

# Displaying the results
print("Square root of", number, "is:", result_sqrt)
print("2 to the power of 3 is:", result_pow)

import math
everything = dir(math)
pint(everything)

def biggest_number(*args):
  print max(args)
  return max(args)
    
def smallest_number(*args):
  print min(args)
  return min(args)

def distance_from_zero(arg):
  print abs(arg)
  return abs(arg)

biggest_number(-10, -5, 5, 10)
smallest_number(-10, -5, 5, 10)
distance_from_zero(-10)

absolute = abs(-42)
print(absolute)

def shut_down(s):
  if s == "yes":
    return "Shutting down"
  elif s == "no":
    return "Shutdown aborted"
  else:
    return "Sorry"

print(shut_down("yes"))

import math
print(math.sqrt(13689))

def distance_from_zero(arg):
    """
    This function takes one argument. If the type is int or float, it returns the absolute value.
    Otherwise, it returns "Nope".
    """
    if type(arg) in [int, float]:
        return abs(arg)
    else:
        return "Nope"

# Examples of using the function
result1 = distance_from_zero(5)
result2 = distance_from_zero(-7.2)
result3 = distance_from_zero("Hello")

print(result1)  # Output: 5 (absolute value of 5)
print(result2)  # Output: 7.2 (absolute value of -7.2)
print(result3)  # Output: Nope (since the input is not int or float)



