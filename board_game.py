from random import randint

# Function to generate a random row and column
def random_row_col(board):
    return randint(0, len(board) - 1), randint(0, len(board[0]) - 1)

# Initialize the board
board_size = 5
board = [["O"] * board_size for _ in range(board_size)]

# Function to print the board
def print_board(board):
    for row in board:
        print(" ".join(row))

# Set the battleship's position
ship_row, ship_col = random_row_col(board)

# Game loop
for _ in range(3):  # Allow 3 guesses
    # Get user input
    guess_row = int(input("Guess Row: "))
    guess_col = int(input("Guess Col: "))

    # Check if the guess is within the valid range
    if 0 <= guess_row < board_size and 0 <= guess_col < board_size:
        # Check the guess
        if guess_row == ship_row and guess_col == ship_col:
            print("Congratulations! You sank my battleship!")
            break
        else:
            print("You missed my battleship!")
            board[guess_row][guess_col] = "X"
            print_board(board)
    else:
        print("Oops, that's not even in the ocean! Try again.")

else:
    print("Game Over. The battleship was at row {} and col {}.".format(ship_row, ship_col))

# Repeated for each code snippet
board = [["O"] * 5 for _ in range(5)]
print_board(board)

ship_row = random_row_col(board)[0]
ship_col = random_row_col(board)[1]
print(ship_row, ship_col)

guess_row = int(input("Guess Row: "))
guess_col = int(input("Guess Col: "))

if guess_row == ship_row and guess_col == ship_col:
    print("Congratulations! You sank my battleship!")
else:
    if guess_row not in range(5) or guess_col not in range(5):
        print("Oops, that's not even in the ocean.")
    else:
        print("You missed my battleship!")
        board[guess_row][guess_col] = "X"
    print_board(board)
