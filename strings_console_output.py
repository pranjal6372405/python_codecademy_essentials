my_string = "hey"
print(len(my_string))
print(my_string.upper())

# strings & console-output

from datetime import datetime
now = datetime.now()

# print('%02d/%02d/%04d %02d:%02d:%02d' % (now.month, now.day, now.year, now.hour, now.minute, now.second)

from datetime import datetime
now = datetime.now()

current_year = now.year
current_month = now.month
current_day = now.day

#Conditionals & Control Flow

# Complete the if and elif statements!
# def grade_converter(grade):
#     if _____:
#         return "A"
#     elif _____:
#         return "B"
#     elif _____:
#         return "C"
#     elif _____:
#         return "D"
#     else:
#         return "F"
      
# This should print an "A"      
# print(grade_converter(92))

# This should print a "C"
# print(grade_converter(70))

# This should print an "F"
# print(grade_converter(61))

def greater_less_equal_5(answer):
    if answer > 5:
        return 1
    elif answer < 5:          
        return -1
    else:
        return 0
        
print(greater_less_equal_5(4))
print(greater_less_equal_5(5))
print(greater_less_equal_5(6))

answer = "'Tis but a scratch!"

def black_knight():
    if answer == "'Tis but a scratch!":
        return True
    else:             
        return False       # Make sure this returns False

def french_soldier():
    if answer == "Go away, or I shall taunt you a second time!":
        return True
    else:             
        return False       # Make sure this returns False
        
        
def using_control_once():
    if True:
        return "Success #1"

def using_control_again():
    if True:
        return "Success #2"

print(using_control_once())
print(using_control_again())

def clinic():
    print("You've just entered the clinic!")
    print("Do you take the door on the left or the right?")
    answer = raw_input("Type left or right and hit 'Enter'.").lower()
    if answer == "left" or answer == "l":
        print("This is the Verbal Abuse Room, you heap of parrot droppings!")
    elif answer == "right" or answer == "r":
        print("Of course this is the Argument Room, I've told you that already!")
    else:
        print("You didn't pick left or right! Try again.")
        clinic()

clinic()

# Use boolean expressions as appropriate on the lines below!

# Make me false!
bool_one_1 = (2 <= 2) and "Alpha" == "Bravo"  # We did this one for you!

# Make me true!
bool_two_1 = True and True

# Make me false!
bool_three_1 = False and True

# Make me true!
bool_four_1 = not False

# Make me true!
bool_five_1 = True or True

bool_one_2 = False or not True and True

bool_two_2 = False and not True or True

bool_three_2 = True and not (False or False)

bool_four_1 = not not True or False and not True

bool_five_1 = False or not (True and True)