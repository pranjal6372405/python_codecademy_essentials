#conditionals and control flow : 

# just define the following conditions with the below given ranges 
#  if((score > 90)&(score < 100)) => Example
# def grade_converter(grade):
#     if ____:
#         return "A"
#     elif ____:
#         return "B"
#     elif ____:
#         return "C"
#     elif ____:
#         return "D"
#     else:
#         return "F"
# print(grade_converter(92))
# print(grade_converter(70))
# print(grade_converter(61))

def greater_less_equal_5(answer):
    if answer > 5:
        return 1
    elif answer < 5:
        return -1
    else:
        return 0
print(greater_less_equal_5(4))
print(greater_less_equal_5(5))
print(greater_less_equal_5(6))

def using_control_once():
    if True:
        return "Success #1"

def using_control_again():
    if True:
        return "Success #2"

print(using_control_once())
print(using_control_again())

bool_one = not True
bool_two = not 3**4 < 4**3
bool_three = not 10 % 3 <= 10 % 2
bool_four = not 3**2 + 4**2 != 5**2
bool_five = not not False

bool_one_power = 2**3 == 108 % 100 or 'Cleese' == 'King Arthur'
bool_two_or = True or False
bool_three_greaterThan = 100**0.5 >= 50 or False
bool_four_or = True or True
bool_five_power_equalsto = 1**100 == 100**1 or 3 * 2 * 1 != 3 + 2 + 1

bool_one_and = False and False
bool_two_subtract_and = -(-(-(-2))) == -2 and 4 >= 16**0.5
bool_three_modulo_and = 19 % 4 != 300 / 10 / 10 and False
bool_four_power_and = -(1**2) < 2**0 and 10 % 10 <= 20 - 10 * 2
bool_five_and = True and True

# Create comparative statements as appropriate on the lines below!

# Make me true!
bool_one_lessthan = 3 < 5  # We already did this one for you!
# Make me false!
bool_two_lessthan = 4 <3
# Make me true!
bool_three_equalsto = 3==3
# Make me false!
bool_four_greaterthan = 18 > 19
# Make me true!
bool_five_equalsto = 3==3

# Assign True or False as appropriate on the lines below!

# (20 - 10) > 15
# bool_one = False    # We did this one for you!
# (10 + 17) == 3**16
# Remember that ** can be read as 'to the power of'. 3**16 is about 43 million.
# bool_two = False
# 1**2 <= -1
# bool_three = False
# 40 * 4 >= -4
# bool_four = True
# 100 != 10**2
# bool_five = False

# Assign True or False as appropriate on the lines below!

# Set this to True if 17 < 328 or to False if it is not.
# bool_one = True   # We did this one for you!
# Set this to True if 100 == (2 * 50) or to False otherwise.
# bool_two = True
# Set this to True if 19 <= 19 or to False if it is not.
# bool_three = True
# Set this to True if -22 >= -18 or to False if it is not.
# bool_four = False
# Set this to True if 99 != (98 + 1) or to False otherwise.
# bool_five = False

def clinic():
    print "You've just entered the clinic!"
    print "Do you take the door on the left or the right?"
    answer = raw_input("Type left or right and hit 'Enter'.").lower()
    if answer == "left" or answer == "l":
        print "This is the Verbal Abuse Room, you heap of parrot droppings!"
    elif answer == "right" or answer == "r":
        print "Of course this is the Argument Room, I've told you that already!"
    else:
        print "You didn't pick left or right! Try again."
        clinic()

clinic()


